package BA2Smoke.BA2Smoke.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "Feature/BA2Smoke.feature"	,
glue={"BA2Smoke.BA2Smoke.steps"} ,  
plugin={"pretty" , "html:target/cucumber-html-report"} , 
//plugin={"pretty" , "json:target/report.json"} ,
monochrome =true , strict= true, dryRun= false)

public class TestRunner {
	
}