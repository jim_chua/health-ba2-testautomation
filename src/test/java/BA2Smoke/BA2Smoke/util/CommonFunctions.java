package BA2Smoke.BA2Smoke.util;

import java.awt.Robot;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;


import BA2Smoke.BA2Smoke.util.TestBase;

public class CommonFunctions extends TestBase {

	public static String resultmssg = null;
	
	public static void Validate(WebElement element, String Exp_val)
	{
		
		resultmssg = null;
		
		//This finds the object and returns the webelemnt object
		WebElement Object_name= element; 
		String Expval = Exp_val;
		String ActValue = Object_name.getText();
		if (ActValue.equalsIgnoreCase(Expval))
		{
			resultmssg = "Exp : "+Expval+" and act : "+ActValue+" value matched!!";
		} else {
			resultmssg = "Exp : "+Expval+" and act : "+ActValue+" value not matched!!";
		}
		System.out.println(resultmssg);
	}
	
	public static void enterText(WebElement element, String ValueToEnter) {
		
		//	System.out.println("ValueToEnter "+ValueToEnter);
		element.clear();

		//enter the value in the textfield using sendkeys
		element.sendKeys(ValueToEnter);
		
		//check if the value is correctly entered
		boolean vluecheck1 = validateTextfieldvalue(element,ValueToEnter);

		//if correct value is not entered
		if(vluecheck1!=true){
			//Filling value again if the value is not entered
			Actions builder = new Actions(driver);
			//enter value using actions class
			builder.moveToElement(element).sendKeys(ValueToEnter).build().perform();
			//check if correct value is entered
			boolean vluecheck2 = validateTextfieldvalue(element,ValueToEnter);
			//if correct value is not entered using actions class also
			if(vluecheck2!=true){
				//enter the text using robot class
				typeCharacter(ValueToEnter);
				staticWait(1);
				//check if the correct value is entered using robot class
				boolean vluecheck3 = validateTextfieldvalue(element,ValueToEnter);
				//validating entered value
				//if correct value is not entered using robot class also
				if(vluecheck3!=true){
					//check if object is disabled if yes do logging
					if (element.isEnabled()==false){
						System.out.println("The required textbox is not enabled therefore "+ ValueToEnter + " cannot be entered");
					}
				}
			}
		}
			
	}

	public static boolean validateTextfieldvalue(WebElement ele, String vl){
		boolean result =false;
		//gets the value for the webelement and stores in a variable
		
		String attvalue = ele.getAttribute("value");
		//if value supplied as parameter and the current value for the webelement matches then set status as passed
		//and return the result as true otherwise false
		
		if(attvalue.equalsIgnoreCase(vl)){
			result = true;
		}
		return result ; 
	}

	public static void typeCharacter(String s){
		try{
			//create an object for the robot class
			Robot robot = new Robot();
			byte[] bytes = s.getBytes();
			for (byte b : bytes) {
				int code = b;
				// keycode only handles [A-Z] (which is ASCII decimal [65-90])
				if (code > 96 && code < 123)
					code = code - 32;
				robot.delay(40);
				robot.keyPress(code);
				robot.keyRelease(code);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void staticWait(int time){
		try{
			System.out.println("Total static wait time "+time);
			for(int i=time; i>0; i--){
				Thread.sleep(5000);
				if(i%5==0){
					System.out.println("System waiting for the element.."+i+" sec to appear!!");
				}
			}
		}catch(Exception e){
			e.getMessage();
		}
	}

}