package BA2Smoke.BA2Smoke.util;

import java.io.FileInputStream;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.By;

public class TestBase {
	
	public static Properties CONFIG = null;
	
	public static WebDriver driver = null;
	public static boolean isBrowserOpen = false;
	public static String browserName;
	
		
	public void initialize() 
	{
		try{
		
			CONFIG = new Properties();
			FileInputStream fistr = new FileInputStream("Configuration\\Config.property");
			CONFIG.load(fistr);
			
			System.out.println(CONFIG.getProperty("url"));
		
		}
		catch(Exception ex)
		{
			System.out.println("Exception while initializing properties files."+ex.getMessage());
		}
	}
	
	public void openBrowser()
	{
	
		browserName = CONFIG.getProperty("browser");
		
		if(!isBrowserOpen)
		{
			if(browserName.equalsIgnoreCase("Mozilla"))
			{
				driver = new FirefoxDriver();
			}
			else if(browserName.equalsIgnoreCase("Chrome"))
			{
				
				String exePath = "browser\\chromedriver.exe";
				String logPath = "chrome.log";
				
				System.setProperty("webdriver.chrome.driver", exePath);
				System.setProperty("webdriver.chrome.logfile", logPath);
				
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
				capabilities.setCapability("applicationCacheEnabled", false);
				
				ChromeOptions opts = new ChromeOptions();
				
				opts.addArguments("--headless");
				opts.addArguments("start-maximized");
				opts.addArguments("--disable-infobars"); // disabling infobars
				opts.addArguments("--disable-extensions"); // disabling extensions
				opts.addArguments("--disable-gpu"); // applicable to windows os only
				opts.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems

				capabilities.setCapability(ChromeOptions.CAPABILITY, opts);

				driver = new ChromeDriver(capabilities);
				driver.manage().deleteAllCookies();
			
	            driver.get(CONFIG.getProperty("url"));

			}
						
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
			isBrowserOpen = true;
		}
	}
	
	public void clickButton(WebElement element) throws InterruptedException {
		System.out.println(element.toString()+" is being clicked now");
		Wait<WebDriver> wait = null;
		
		try {
		       wait = new FluentWait<WebDriver>((WebDriver) driver)
		    		   .withTimeout(30, TimeUnit.SECONDS)
		    		   .pollingEvery(3, TimeUnit.SECONDS);
		        wait.until(ExpectedConditions.visibilityOf(element));
		        
		        //System.out.println(element.getText());
		        
		        Actions actions = new Actions(driver);
		        actions.moveToElement(element).click().perform();

		        System.out.println("Waiting for "+element.toString());
		    }
		catch(Exception e) {
		}
	}
}