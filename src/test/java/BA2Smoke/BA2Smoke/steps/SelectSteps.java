package BA2Smoke.BA2Smoke.steps;

import java.io.File;

import BA2Smoke.BA2Smoke.pages.*;
import BA2Smoke.BA2Smoke.util.TestBase;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SelectSteps extends TestBase	 {
	
	LoginPage loginPage;
	HomePage homePage;
	EnrolmentPage enrolmentPage;
	EnrolmentStatementPage enrolmentStatement;
	DetailsPage detailsPage;
	SOAPage soaPage;
	BenefitsPage benefitsPage;
	ClaimsPage claimsPage;
	
	public static String target;
	
	public static String user;
	public static String pass;
	
	@Given("^user launched the browser$")
	public void user_launched_the_browser() throws Throwable {
			initialize();
			openBrowser();
	}

	@When("^user enter username and password$")
	public void enter_username_and_password() throws Throwable {
		loginPage = new LoginPage();
		
		user = CONFIG.getProperty("username");
		pass = CONFIG.getProperty("password");
		
		loginPage.login(user, pass);
	}

	@Then("^user is on the home page$")
	public void employee_portal_Home_Page () throws Throwable {
		homePage = new HomePage();
		homePage.validateHomePage();
	}

	@Then("^user logout$")
	public void closeBrowser() throws Throwable {
		homePage = new HomePage();
		homePage.logout();
	}
	
	@Given("^user click on \"([^\"]*)\"$")
	public void user_clicked_on(String arg1) throws Throwable {
		
		target = arg1;
		
		if (target.equalsIgnoreCase("home")){
			homePage = new HomePage();
			homePage.home();
						
		} else if (target.equalsIgnoreCase("start enrolment")) {
			homePage = new HomePage();
			homePage.enrolment();
			System.out.println("\nStarting Enrolment\n");
			
		} else if (target.equalsIgnoreCase("enrolment statement")) {
			enrolmentPage = new EnrolmentPage();
			enrolmentPage.enrollment_statement();
			System.out.println("\nView Enrolment Statement\n");
			
		} else if (target.equalsIgnoreCase("My Details")) {
			homePage = new HomePage();
			homePage.details();
			System.out.println("\nMy Details\n");
			
		} else if (target.equalsIgnoreCase("soa")) {
			homePage = new HomePage();
			homePage.soa();
			System.out.println("\nSOA\n");
			
		} else if (target.equalsIgnoreCase("my benefits")) {
			homePage = new HomePage();
			homePage.benefits();
			System.out.println("\nMy Benefits\n");
			
		}  else if (target.equalsIgnoreCase("claims")) {
			homePage = new HomePage();
			homePage.claims();
			System.out.println("\nClaims\n");
			
		}
	}
	
	@Then("^user is on the \"([^\"]*)\" page$")
	public void user_is_on_the_page(String arg1) throws Throwable {
		
		target = arg1;
		
		if (target.equalsIgnoreCase("enrolment")) {
			enrolmentPage = new EnrolmentPage();
			enrolmentPage.validateEnrolmentPage();
			
		} else if (target.equalsIgnoreCase("enrolment statement")) {
			enrolmentStatement = new EnrolmentStatementPage();
			enrolmentStatement.validateEnrolmentStatementPage();
			
		} else if (target.equalsIgnoreCase("personal details")) {
			detailsPage = new DetailsPage();
			detailsPage.validateDetailsPage(user);
			
		} else if (target.equalsIgnoreCase("soa")) {
			soaPage = new SOAPage();
			soaPage.validateEEName(user);
			
		} else if (target.equalsIgnoreCase("my benefits")) {
			benefitsPage = new BenefitsPage();
			benefitsPage.validateBenefitsInformationLabel();
			
		} else if (target.equalsIgnoreCase("claims")) {
			claimsPage = new ClaimsPage();
			claimsPage.validateSearchClaimsLink();
			
		}
	}
}
