package BA2Smoke.BA2Smoke.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BA2Smoke.BA2Smoke.util.CommonFunctions;
import BA2Smoke.BA2Smoke.util.TestBase;

public class EnrolmentPage extends TestBase {

	@FindBy(xpath = "//a[contains(text(),'My Enrollment')]")
	WebElement enrolmentLabel;
	
	@FindBy(xpath = "//a[contains(text(),'Enrollment Statement')]")
	WebElement enrolmentStatement;
	
	
	public EnrolmentPage(){
		PageFactory.initElements(driver, this);
	}

	public void validateEnrolmentPage() throws InterruptedException{
		CommonFunctions.Validate(enrolmentLabel, "My Enrollment");
	}
	
	public EnrolmentStatementPage enrollment_statement() throws InterruptedException{
		clickButton(enrolmentStatement);		
		return new EnrolmentStatementPage(); 
	}
}