package BA2Smoke.BA2Smoke.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BA2Smoke.BA2Smoke.util.CommonFunctions;
import BA2Smoke.BA2Smoke.util.TestBase;

public class BenefitsPage extends TestBase {

	@FindBy(xpath = "//span[text() = 'My Benefits' and contains(@id, '_MyBenefits_')]")
	WebElement benefitsLabel;
	
	@FindBy(xpath = "//*[contains(text(), 'benefits information') and contains(@class,'accordion')]")
	WebElement benefitsInformationLabel;
	
	public BenefitsPage(){
		PageFactory.initElements(driver, this);
	}
	
	public void validateBenefitsInformationLabel() throws InterruptedException{
		
		//user = CONFIG.getProperty("username");
		
		CommonFunctions.Validate(benefitsInformationLabel, "benefits information");
		
	}

}