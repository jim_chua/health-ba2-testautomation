package BA2Smoke.BA2Smoke.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BA2Smoke.BA2Smoke.util.CommonFunctions;
import BA2Smoke.BA2Smoke.util.TestBase;

public class ClaimsPage extends TestBase {

	@FindBy(xpath = "//a[contains(@id, 'SearchClaim') and contains(@href,'ClaimsSearch') and contains(@class,'golink')]")
	WebElement searchClaimLink;
	
	public ClaimsPage(){
		PageFactory.initElements(driver, this);
	}
	
	public void validateSearchClaimsLink() throws InterruptedException{
		
		//user = CONFIG.getProperty("username");
		
		CommonFunctions.Validate(searchClaimLink, "go");
		
	}

}