package BA2Smoke.BA2Smoke.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BA2Smoke.BA2Smoke.util.CommonFunctions;
import BA2Smoke.BA2Smoke.util.TestBase;

public class SOAPage extends TestBase {

	@FindBy(xpath = "//h1[contains(text(),'Statement of Account')]")
	WebElement SOALabel;
	
	@FindBy(xpath = "//span[contains(text(),'jimtest01Oct_ee01_fn')]")
	WebElement employeeFullName;
	
	
	public SOAPage(){
		PageFactory.initElements(driver, this);
	}

	public void validateEEName(String user) throws InterruptedException{
		
		//user = CONFIG.getProperty("username");
		
		CommonFunctions.Validate(employeeFullName, user);
	}
}