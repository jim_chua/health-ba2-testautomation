package BA2Smoke.BA2Smoke.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BA2Smoke.BA2Smoke.util.CommonFunctions;
import BA2Smoke.BA2Smoke.util.TestBase;

import BA2Smoke.BA2Smoke.pages.*;

public class HomePage extends TestBase {

	//Top Menu
	@FindBy(xpath="//*[contains(@href,'/ba2web_wip/simpliflexsg/Home')]")
	WebElement homePageButton;
	
	@FindBy(xpath="//span[contains(text(),'Start Enrollment')]")
	WebElement enrolmentButton;
	
	@FindBy(xpath = "//span[contains(text(),'My Details')]")
	WebElement myDetailsButton;
	
	@FindBy(xpath = "//span[contains(text(),'Statement of Account')]")
	WebElement SOAButton;
	
	@FindBy(xpath = "//span[contains(text(),'My Benefits')]")
	WebElement myBenefitsButton;
	
	@FindBy(xpath = "//span[contains(text(),'Claims')]")
	WebElement claimsButton;
	
	
	//Links
	@FindBy(xpath = "//*[contains(@id, '_lbtnChangePassword')]")
	WebElement changePasswordLink;
	
	
	@FindBy(xpath = "//*[contains(@id, '_dnnLOGIN_cmdLogin')]")
	WebElement logoutLink;
	
	
	//Initializing the page objects
	public HomePage(){
		PageFactory.initElements(driver, this);
	}
	
	public void validateHomePage() throws InterruptedException{
		CommonFunctions.Validate(enrolmentButton, "Start Enrollment");
		CommonFunctions.Validate(myDetailsButton, "My Details");
		CommonFunctions.Validate(SOAButton, "Statement of Account");
		CommonFunctions.Validate(myBenefitsButton, "My Benefits");
		CommonFunctions.Validate(claimsButton, "Claims");
		CommonFunctions.Validate(changePasswordLink, "Change Password");
		CommonFunctions.Validate(logoutLink, "Logout");
	}
	
	public void home() throws InterruptedException{
		clickButton(homePageButton);
		//return home();
	}
	
	public EnrolmentPage enrolment() throws InterruptedException{
		clickButton(enrolmentButton);
		return new EnrolmentPage();
	}
	
	public DetailsPage details() throws InterruptedException{
		
		clickButton(myDetailsButton);
		return new DetailsPage();
	}
	
	public SOAPage soa() throws InterruptedException{
		
		clickButton(SOAButton);
		return new SOAPage();
	}
	
	public BenefitsPage benefits() throws InterruptedException{
		
		clickButton(myBenefitsButton);
		return new BenefitsPage();
	}
	
	public ClaimsPage claims() throws InterruptedException{
		
		clickButton(claimsButton);
		return new ClaimsPage();
	}
	
	public void logout() throws InterruptedException{
		
		clickButton(logoutLink);
		CommonFunctions.staticWait(2);
		driver.quit();
	} 
	
}
