package BA2Smoke.BA2Smoke.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BA2Smoke.BA2Smoke.util.CommonFunctions;
import BA2Smoke.BA2Smoke.util.TestBase;

public class EnrolmentStatementPage extends TestBase {
	
	@FindBy(xpath = "//span[contains(text(),'Enrollment Statement')]")
	WebElement enrolmentStatementLabel;
	
	public EnrolmentStatementPage(){
		PageFactory.initElements(driver, this);
	}

	public void validateEnrolmentStatementPage() throws InterruptedException{
		CommonFunctions.Validate(enrolmentStatementLabel, "Enrollment Statement");
	}
}