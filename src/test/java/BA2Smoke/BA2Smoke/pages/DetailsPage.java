package BA2Smoke.BA2Smoke.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BA2Smoke.BA2Smoke.util.CommonFunctions;
import BA2Smoke.BA2Smoke.util.TestBase;

public class DetailsPage extends TestBase {

	@FindBy(xpath = "//span[contains(text(),'Personal Details')]")
	WebElement personalDetailsLabel;
	
	@FindBy(xpath = "//span[contains(text(),'jimtest01Oct_ee01_un')]")
	WebElement employeeNameLabel;
	
	public DetailsPage(){
		PageFactory.initElements(driver, this);
	}

	public void validateDetailsPage(String user) throws InterruptedException{
		
		//user = CONFIG.getProperty("username");
		
		CommonFunctions.Validate(employeeNameLabel, user);
	}
}