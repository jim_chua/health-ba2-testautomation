package BA2Smoke.BA2Smoke.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import BA2Smoke.BA2Smoke.util.CommonFunctions;
import BA2Smoke.BA2Smoke.util.TestBase;

public class LoginPage extends TestBase {
	
	//Page Factory- OR:
	@FindBy(xpath = "//input[contains(@id, '_SignIn_v2_txtUsername')]")
	WebElement username;
	
	@FindBy(xpath = "//input[contains(@id, '_SignIn_v2_txtPassword')]")
	WebElement password;
	
	@FindBy(xpath = "//input[contains(@id, '_SignIn_v2_cmdLogin')]")
	WebElement loginButton;
	
	
	public LoginPage(){
		PageFactory.initElements(driver, this);
	}
	
	
	public HomePage login(String user, String pass) throws InterruptedException{
	
		clickButton(username);
		CommonFunctions.enterText(username, user);
		
		clickButton(password);
		CommonFunctions.enterText(password, pass);
		
		clickButton(loginButton);
		
		return new HomePage();
		
	}

}
