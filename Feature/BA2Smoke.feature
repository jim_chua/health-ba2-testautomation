Feature: Verify the Employee Portal login page

@Smoke
Scenario: Verify home page loading and login
Given user launched the browser
When user enter username and password
Then user is on the home page

@Smoke
Scenario: Verify employee is able to open My Enrolment
Given user click on "home"
When user click on "Start Enrolment"
Then user is on the "enrolment" page
When user click on "Enrolment Statement"
Then user is on the "enrolment statement" page

@Smoke
Scenario: Verify employee is able to open My Details
Given user click on "home"
When user click on "My Details"
Then user is on the "personal details" page

@Smoke
Scenario: Verify employee is able to open SOA
Given user click on "home"
When user click on "SOA"
Then user is on the "SOA" page

@Smoke
Scenario: Verify employee is able to open My Benefits
Given user click on "home"
When user click on "My Benefits"
Then user is on the "my benefits" page

@Smoke
Scenario: Verify employee is able to open Claims
Given user click on "home"
When user click on "Claims"
Then user is on the "claims" page
Then user logout
