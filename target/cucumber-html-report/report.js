$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Feature/BA2Smoke.feature");
formatter.feature({
  "line": 1,
  "name": "Verify the Employee Portal login page",
  "description": "",
  "id": "verify-the-employee-portal-login-page",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "Verify home page loading and login",
  "description": "",
  "id": "verify-the-employee-portal-login-page;verify-home-page-loading-and-login",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Smoke"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "user launched the browser",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user enter username and password",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user is on the home page",
  "keyword": "Then "
});
formatter.match({
  "location": "SelectSteps.user_launched_the_browser()"
});
formatter.result({
  "duration": 3764859890,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.enter_username_and_password()"
});
formatter.result({
  "duration": 31409738855,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.employee_portal_Home_Page()"
});
formatter.result({
  "duration": 441967604,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Verify employee is able to open My Enrolment",
  "description": "",
  "id": "verify-the-employee-portal-login-page;verify-employee-is-able-to-open-my-enrolment",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 9,
      "name": "@Smoke"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "user click on \"home\"",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "user click on \"Start Enrolment\"",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "user is on the \"enrolment\" page",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "user click on \"Enrolment Statement\"",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "user is on the \"enrolment statement\" page",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "home",
      "offset": 15
    }
  ],
  "location": "SelectSteps.user_clicked_on(String)"
});
formatter.result({
  "duration": 371554977,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Start Enrolment",
      "offset": 15
    }
  ],
  "location": "SelectSteps.user_clicked_on(String)"
});
formatter.result({
  "duration": 1574935327,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "enrolment",
      "offset": 16
    }
  ],
  "location": "SelectSteps.user_is_on_the_page(String)"
});
formatter.result({
  "duration": 69247656,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Enrolment Statement",
      "offset": 15
    }
  ],
  "location": "SelectSteps.user_clicked_on(String)"
});
formatter.result({
  "duration": 1776768171,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "enrolment statement",
      "offset": 16
    }
  ],
  "location": "SelectSteps.user_is_on_the_page(String)"
});
formatter.result({
  "duration": 72042244,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Verify employee is able to open My Details",
  "description": "",
  "id": "verify-the-employee-portal-login-page;verify-employee-is-able-to-open-my-details",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 17,
      "name": "@Smoke"
    }
  ]
});
formatter.step({
  "line": 19,
  "name": "user click on \"home\"",
  "keyword": "Given "
});
formatter.step({
  "line": 20,
  "name": "user click on \"My Details\"",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "user is on the \"personal details\" page",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "home",
      "offset": 15
    }
  ],
  "location": "SelectSteps.user_clicked_on(String)"
});
formatter.result({
  "duration": 395986990,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "My Details",
      "offset": 15
    }
  ],
  "location": "SelectSteps.user_clicked_on(String)"
});
formatter.result({
  "duration": 551543142,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "personal details",
      "offset": 16
    }
  ],
  "location": "SelectSteps.user_is_on_the_page(String)"
});
formatter.result({
  "duration": 46230110,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Verify employee is able to open SOA",
  "description": "",
  "id": "verify-the-employee-portal-login-page;verify-employee-is-able-to-open-soa",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 23,
      "name": "@Smoke"
    }
  ]
});
formatter.step({
  "line": 25,
  "name": "user click on \"home\"",
  "keyword": "Given "
});
formatter.step({
  "line": 26,
  "name": "user click on \"SOA\"",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "user is on the \"SOA\" page",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "home",
      "offset": 15
    }
  ],
  "location": "SelectSteps.user_clicked_on(String)"
});
formatter.result({
  "duration": 357585199,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "SOA",
      "offset": 15
    }
  ],
  "location": "SelectSteps.user_clicked_on(String)"
});
formatter.result({
  "duration": 646934145,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "SOA",
      "offset": 16
    }
  ],
  "location": "SelectSteps.user_is_on_the_page(String)"
});
formatter.result({
  "duration": 69257130,
  "status": "passed"
});
formatter.scenario({
  "line": 30,
  "name": "Verify employee is able to open My Benefits",
  "description": "",
  "id": "verify-the-employee-portal-login-page;verify-employee-is-able-to-open-my-benefits",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 29,
      "name": "@Smoke"
    }
  ]
});
formatter.step({
  "line": 31,
  "name": "user click on \"home\"",
  "keyword": "Given "
});
formatter.step({
  "line": 32,
  "name": "user click on \"My Benefits\"",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "user is on the \"my benefits\" page",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "home",
      "offset": 15
    }
  ],
  "location": "SelectSteps.user_clicked_on(String)"
});
formatter.result({
  "duration": 327982074,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "My Benefits",
      "offset": 15
    }
  ],
  "location": "SelectSteps.user_clicked_on(String)"
});
formatter.result({
  "duration": 631326854,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "my benefits",
      "offset": 16
    }
  ],
  "location": "SelectSteps.user_is_on_the_page(String)"
});
formatter.result({
  "duration": 45914293,
  "status": "passed"
});
formatter.scenario({
  "line": 36,
  "name": "Verify employee is able to open Claims",
  "description": "",
  "id": "verify-the-employee-portal-login-page;verify-employee-is-able-to-open-claims",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 35,
      "name": "@Smoke"
    }
  ]
});
formatter.step({
  "line": 37,
  "name": "user click on \"home\"",
  "keyword": "Given "
});
formatter.step({
  "line": 38,
  "name": "user click on \"Claims\"",
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "user is on the \"claims\" page",
  "keyword": "Then "
});
formatter.step({
  "line": 40,
  "name": "user logout",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "home",
      "offset": 15
    }
  ],
  "location": "SelectSteps.user_clicked_on(String)"
});
formatter.result({
  "duration": 321009620,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Claims",
      "offset": 15
    }
  ],
  "location": "SelectSteps.user_clicked_on(String)"
});
formatter.result({
  "duration": 482965413,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "claims",
      "offset": 16
    }
  ],
  "location": "SelectSteps.user_is_on_the_page(String)"
});
formatter.result({
  "duration": 50498774,
  "status": "passed"
});
formatter.match({
  "location": "SelectSteps.closeBrowser()"
});
formatter.result({
  "duration": 11609638504,
  "status": "passed"
});
});